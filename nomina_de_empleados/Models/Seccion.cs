﻿using System.ComponentModel.DataAnnotations;

namespace nomina_de_empleados.Models
{
    public class Seccion
    {
        [Key]
        public int ID { get; }

        public int Numero { get; set; }
    }
}