﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace nomina_de_empleados.Models
{
    public class Tipo
    {
        [Key]
        public int ID { get; }

        public string Descripcion { get; set; } 
    }
}
