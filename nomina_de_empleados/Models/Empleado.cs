﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity;
using System.Data.SqlClient;

namespace nomina_de_empleados.Models
{
    public class Empleado
    {
        [Key]
        public int ID { get; }

        [Required]
        [Display(Name = "Nombre")]
        public string Nombres { get; set; }

        [Required]
        [Display(Name = "Apellidos")]
        public string Apellidos { get; set; }

        [Required]
        [Display(Name = "Horas")]
        public decimal Horas { get; set; }

        [Required]
        [Display(Name = "Importe")]
        public decimal Importe { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public Tipo Tipo { get; set; }

        [Required]
        [Display(Name = "Seccion")]
        public Seccion Seccion { get; set; }
    }

    public class EmpleadoDbContext : DbContext
    {
        public EmpleadoDbContext()
            : base("name = DefaultConnection")
        {
        }

        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<Seccion> Secciones { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
        
    }
}