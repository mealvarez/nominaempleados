﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nomina_de_empleados.Models;

namespace nomina_de_empleados.Controllers
{
    public class NominaController : Controller
    {
        private EmpleadoDbContext db = new EmpleadoDbContext();

        public ActionResult Index()
        {
            var coneccion = db.Database.Connection;
            //recuperar empleados base de datos.
            var _empleados = db.Empleados.ToList();

            return View(_empleados);
        }
    }
}